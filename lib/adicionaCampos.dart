import 'package:flutter/material.dart';
import 'package:validacao_dinamica/helper.dart';
import 'package:validacao_dinamica/topografias_list.dart';

List<DynamicWidget> dynamicWidgets = List<DynamicWidget>();

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  update() {
    setState(() {
      //dynamicWidgets.removeAt(index);
    });
  }

  addDynamic() {
    if (dynamicWidgets.isEmpty) {
      dynamicWidgets.add(DynamicWidget(
        update: update(),
        posicaoLista: 0,
      ));
      setState(() {});
    } else {
      var index = dynamicWidgets.indexOf(dynamicWidgets.last);
      print(index);

      dynamicWidgets.add(DynamicWidget(
        posicaoLista: index + 1,
        update: update(),
      ));
      setState(() {});
    }
  }

  submitData() async {
    dynamicWidgets.forEach((widget) {
      print(widget.controllerName.text);
      print(widget.controllerAge.text);
    });

    DatabaseHelper dbClient = DatabaseHelper();
    var idTopog = await dbClient.saveTopog("");

    dynamicWidgets.forEach((widget) async {
      await dbClient.saveData(
          widget.controllerName.text, widget.controllerAge.text, idTopog);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Validação de campos"),
        ),
        body: Container(
          child: Column(
            children: [
              Flexible(
                child: ListView.builder(
                  itemCount: dynamicWidgets.length,
                  itemBuilder: (_, index) => dynamicWidgets[index],
                ),
              ),
              Container(
                child: RaisedButton(
                  onPressed: submitData,
                  child: Text("Salvar"),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 15.0, bottom: 30, left: 30, right: 30),
                child: RaisedButton(
                  color: Colors.blue,
                  child: const Text(
                    'Editar campos',
                  ), //style: buttonText),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => TopografiasList()),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: addDynamic,
          child: Icon(Icons.add),
        ),
      ),
    );
  }
}

class DynamicWidget extends StatefulWidget {
  final dynamic posicaoLista;
  final ValueChanged<dynamic> update;
  final TextEditingController controllerName;
  final TextEditingController controllerAge;

  const DynamicWidget(
      {Key key,
      this.posicaoLista,
      this.controllerName,
      this.controllerAge,
      this.update})
      : super(key: key);

  @override
  _DynamicWidgetState createState() => _DynamicWidgetState();
}

class _DynamicWidgetState extends State<DynamicWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.all(8),
          child: TextField(
            controller: widget.controllerName,
            decoration: InputDecoration(
              hintText: "Enter Name",
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.all(8),
          child: TextField(
            controller: widget.controllerAge,
            decoration: InputDecoration(
              hintText: "Enter Age",
            ),
          ),
        ),
        IconButton(
          icon: new Icon(Icons.delete),
          onPressed: () {
            print("DELETAR");
            dynamicWidgets.removeAt(widget.posicaoLista);
            widget.update(widget.posicaoLista);
          },
        )
      ],
    );
  }
}
