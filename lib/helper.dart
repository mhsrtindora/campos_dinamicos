import 'package:sqflite/sqlite_api.dart';
import 'package:validacao_dinamica/db.dart';

class DatabaseHelper {
  String name;
  String age;

  DatabaseHelper();

  DatabaseHelper.fromMap(dynamic obj) {
    this.name = obj['name'];
    this.age = obj['age'];
  }

  Future saveData(name, age, topogRelated) async {
    Database dbClient = await DatabaseClient().db;

    print("Dados sendo recebidos pela função saveData:");
    print(name);
    print(age);

    await dbClient.rawInsert(
        """INSERT OR REPLACE INTO Topografia_Dados(name, age, topog_related) VALUES (\'$name\', \'$age\', \'$topogRelated\') """);
  }

  Future saveTopog(name) async {
    Database dbClient = await DatabaseClient().db;

    var teste = await dbClient.rawInsert(
        """INSERT OR REPLACE INTO Topografia(topog_name) VALUES (\'$name\')""");
    return teste;
  }

  Future updateDataSaving(name, age, id) async {
    Database dbClient = await DatabaseClient().db;

    dbClient.rawUpdate("""
          UPDATE Topografia_Dados
          SET name = \'$name\', age = \'$age\'
          WHERE id = \'$id\';
        """);
  }

  Future getDataEdition(idRelacionado) async {
    Database dbClient = await DatabaseClient().db;

    var dados = await dbClient.query("Topografia_Dados",
        columns: ["id", "name", "age"],
        where: "topog_related = ?",
        whereArgs: [idRelacionado]);
    return dados;
  }

  Future getData() async {
    Database dbClient = await DatabaseClient().db;

    var dados = await dbClient.rawQuery("""SELECT * FROM Topografia_Dados""");
    return dados;
  }

  Future getTopografias() async {
    Database dbClient = await DatabaseClient().db;

    var dados = await dbClient.rawQuery("""SELECT * FROM Topografia""");
    return dados;
  }

  Future deleteTopografia(id) async {
    Database dbClient = await DatabaseClient().db;

    var delete =
        dbClient.delete('Topografia', where: "id = ?", whereArgs: [id]);
    return delete;
  }
}
