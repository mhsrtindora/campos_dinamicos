import 'dart:async';
import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseClient {
  static final DatabaseClient _instance = new DatabaseClient.internal();

  factory DatabaseClient() => _instance;

  static Database _db;

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await initDB();
    return _db;
  }

  DatabaseClient.internal();

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    var path = join(documentsDirectory.path, "dados_conteudo.db");

    var dbInstance = await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
    );
    return dbInstance;
  }

  void _onCreate(Database db, int version) async {
    await db.execute("""CREATE TABLE Topografia (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        topog_name TEXT)""");
    await db.execute("""CREATE TABLE Topografia_Dados (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT,
        age TEXT,
        topog_related INTEGER)""");
  }
}
