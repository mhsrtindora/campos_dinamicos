import 'package:flutter/material.dart';
import 'package:validacao_dinamica/helper.dart';

List<DynamicFields> dynamicFields = List<DynamicFields>();

class EditTopologia extends StatefulWidget {
  final idRelated;

  const EditTopologia({Key key, this.idRelated}) : super(key: key);

  @override
  _EditTopologiaState createState() => _EditTopologiaState();
}

class _EditTopologiaState extends State<EditTopologia> {
  DatabaseHelper dbClient = DatabaseHelper();

  addDynamic(name) {
    setState(() {
      return dynamicFields.add(DynamicFields(name: name));
    });
  }

  //ATENÇÃO!! A variável names é uma lista do tipo DynamicFields
  updateData(names) async {
    names.forEach((widget) async {
      print("ID:");
      print(widget.id);
      print(widget.name);
      await dbClient.updateDataSaving(widget.name, widget.age, widget.id);
    });
  }

  @override
  void initState() {
    super.initState();
    dynamicFields.clear();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Editar Topografias"),
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back_ios, color: Colors.white),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: Center(
          child: FutureBuilder<dynamic>(
            future: camposForm(widget.idRelated),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return SingleChildScrollView(
                  child: Column(children: [
                    Column(children: snapshot.data),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        RaisedButton(
                          color: Colors.green,
                          child: const Text(
                            'Salvar Edição',
                          ),
                          onPressed: () => updateData(snapshot.data),
                        ),
                      ],
                    ),
                  ]),
                );
              } else {
                return CircularProgressIndicator();
              }
            },
          ),
        ),
      ),
    );
  }

  Future<List> getData(idRelacionado) async {
    var dadosBanco = await dbClient.getDataEdition(idRelacionado);

    return dadosBanco;
  }

  Future camposForm(idRelacionado) async {
    var dadosBanco = await getData(idRelacionado);

    for (int i = 0; i < dadosBanco.length; i++) {
      print(dadosBanco[i]['id']);
      print(dadosBanco[i]['name']);
      print(dadosBanco[i]['age']);

      dynamicFields.add(DynamicFields(
          name: dadosBanco[i]['name'],
          id: dadosBanco[i]['id'],
          age: dadosBanco[i]['age'],
          posicaoLista: i));
    }

    return dynamicFields;
  }
}

class DynamicFields extends StatelessWidget {
  final dynamic posicaoLista;
  final dynamic id;
  var name;
  var age;
  DynamicFields(
      {Key key, @required this.name, this.posicaoLista, this.id, this.age})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.all(8),
          child: TextField(
            controller: TextEditingController(text: name),
            onChanged: (value) => dynamicFields[posicaoLista].name = value,
            decoration: InputDecoration(
              hintText: "Enter Data",
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.all(8),
          child: TextField(
            controller: TextEditingController(text: age),
            onChanged: (value) => dynamicFields[posicaoLista].age = value,
            decoration: InputDecoration(
              hintText: "Enter Data",
            ),
          ),
        ),
      ],
    );
  }
}
