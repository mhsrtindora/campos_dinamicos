import 'package:flutter/material.dart';
import 'package:validacao_dinamica/editTopologia.dart';
import 'package:validacao_dinamica/helper.dart';

class TopografiasList extends StatefulWidget {
  @override
  _TopografiasListState createState() => _TopografiasListState();
}

class _TopografiasListState extends State<TopografiasList> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Topografias"),
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back_ios, color: Colors.white),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: Center(
          child: FutureBuilder<List>(
            future: getData(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Container(
                    child: ListView(
                  children: snapshot.data.map((dadosBanco) {
                    return GestureDetector(
                      child: Card(
                        child: Padding(
                            padding: EdgeInsets.all(10),
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(left: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        "Nome: ${dadosBanco['topog_name']}",
                                        style: TextStyle(fontSize: 16),
                                      ),
                                      Text(
                                        "ID: ${dadosBanco['id']}",
                                        style: TextStyle(fontSize: 16),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            )),
                      ),
                      onTap: () {
                        _showOptions(context, dadosBanco['id']);
                      },
                    );
                  }).toList(),
                ));
              } else {
                return Container();
              }
            },
          ),
        ),
      ),
    );
  }

  DatabaseHelper dbClient = DatabaseHelper();
  Future<List> getData() async {
    var dadosBanco = await dbClient.getTopografias();
    return dadosBanco;
  }

  void _showOptions(BuildContext context, idTopografia) {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return BottomSheet(
          onClosing: () {},
          builder: (context) {
            return Container(
              padding: EdgeInsets.all(10),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(10),
                    child: FlatButton(
                      child: Text(
                        "Editar topografia",
                        style: TextStyle(color: Colors.blue, fontSize: 20),
                      ),
                      onPressed: () async {
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => EditTopologia(
                                    idRelated: idTopografia,
                                  )),
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10),
                    child: FlatButton(
                      child: Text(
                        "Deletar",
                        style: TextStyle(color: Colors.blue, fontSize: 20),
                      ),
                      onPressed: () {
                        var deletar;
                        showDialog(
                          context: context,
                          builder: (context) {
                            return WillPopScope(
                              onWillPop: () async => true,
                              child: AlertDialog(
                                title: Text(
                                  "Deseja deletar a topografia?",
                                  textAlign: TextAlign.center,
                                ),
                                content: Row(
                                  children: <Widget>[
                                    new Expanded(
                                        child: new TextField(
                                      autofocus: true,
                                      decoration: new InputDecoration(
                                          labelText: 'Digite sim para deletar',
                                          hintText: 'sim'),
                                      onChanged: (value) {
                                        setState(() {
                                          deletar = value;
                                        });
                                      },
                                    ))
                                  ],
                                ),
                                actions: <Widget>[
                                  FlatButton(
                                    child: Text("Não"),
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                  ),
                                  FlatButton(
                                    child: Text("Deletar"),
                                    onPressed: () async {
                                      if (deletar == 'sim' ||
                                          deletar == 'Sim') {
                                        print(
                                            "Função de apagar topografia aqui");
                                        await dbClient
                                            .deleteTopografia(idTopografia);
                                        setState(() {});
                                        Navigator.pop(context);
                                        Navigator.pop(context);
                                      }
                                    },
                                  )
                                ],
                              ),
                            );
                          },
                        );
                      },
                    ),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }
}
